---
variables:
  GITLAB_CI_IMAGE_DEBIAN: 'debian:bullseye'
  GITLAB_CI_IMAGE_SENTRY_CLI: 'getsentry/sentry-cli'
  GITLAB_CI_DOCKER_BUILDX_PLATFORMS: 'linux/amd64,linux/arm/v7,linux/arm64'
  GITLAB_CI_DOCKER_IMAGE_NAME: 'satnogs-client'
  GITLAB_CI_DOCKER_PULL: '1'
  GITLAB_CI_DOCKER_IMAGE_TAG_LATEST: '${CI_COMMIT_TAG}'
  GITLAB_CI_APT_REPOSITORY: 'deb [signed-by=/etc/apt/keyrings/satnogs.gpg] http://download.opensuse.org/repositories/home:/librespace:/satnogs/Debian_11/ /'
  GITLAB_CI_APT_KEY_URL: 'https://download.opensuse.org/repositories/home:librespace:satnogs/Debian_11/Release.key'
  GITLAB_CI_APT_PACKAGES: >-
    git
    python3-pip
    python3-venv
  GITLAB_CI_OBS_PROJECT: 'home:librespace:satnogs'
  GITLAB_CI_SIGN_OFF_EXCLUDE: 'a3fe97202e64477cc59554f2f76299e41a3113f7'
stages:
  - static
  - build
  - test
  - deploy
  - sentry_release
  - security

include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
  - template: 'Security/Dependency-Scanning.gitlab-ci.yml'
  - template: 'Security/SAST.gitlab-ci.yml'
  - template: 'Security/Secret-Detection.gitlab-ci.yml'
  - template: 'Security/License-Scanning.gitlab-ci.yml'
  - template: 'Security/Container-Scanning.gitlab-ci.yml'
  - project: 'librespacefoundation/templates/gitlab-ci'
    ref: '2fe58fc924c5b36218193dc2428d702065334b58'
    file: '.gitlab-ci-sign-off.yml'
  - project: 'librespacefoundation/templates/gitlab-ci'
    ref: '2fe58fc924c5b36218193dc2428d702065334b58'
    file: '.gitlab-ci-docs.yml'
  - project: 'librespacefoundation/templates/gitlab-ci'
    ref: '2fe58fc924c5b36218193dc2428d702065334b58'
    file: '.gitlab-ci-docker.yml'

.default_before_script: &default_before_script
  - apt-get -y update
  - apt-get -qy install gnupg libcurl4 wget
  - echo "$GITLAB_CI_APT_REPOSITORY" > /etc/apt/sources.list.d/${GITLAB_CI_OBS_PROJECT}.list
  - mkdir -p /etc/apt/keyrings/
  - wget -O- "$GITLAB_CI_APT_KEY_URL" | gpg --dearmor | tee /etc/apt/keyrings/satnogs.gpg > /dev/null
  - apt-get -q update
  - apt-get -y install $GITLAB_CI_APT_PACKAGES
  - xargs -r -a packages.debian apt-get -qy install
  - pip install -cconstraints.txt tox

# 'static' stage
sign_off:
  stage: static
  needs: []

lint:
  stage: static
  needs: []
  image: ${GITLAB_CI_IMAGE_DEBIAN}
  before_script:
    - *default_before_script
  rules:
    - when: never

static:
  stage: static
  needs: []
  image: ${GITLAB_CI_IMAGE_DEBIAN}
  before_script:
    - *default_before_script
  script:
    - tox run-parallel -e "flake8,isort,yapf,pylint,robot-lint"

# 'build' stage
docs:
  stage: build
  needs: []
  image: ${GITLAB_CI_IMAGE_DEBIAN}
  before_script:
    - *default_before_script

build:
  stage: build
  needs: []
  image: ${GITLAB_CI_IMAGE_DEBIAN}
  before_script:
    - *default_before_script
  script:
    - rm -rf dist
    - tox run -e "build"
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - dist

# 'test' stage
test:
  stage: test
  needs: []
  image: ${GITLAB_CI_IMAGE_DEBIAN}
  before_script:
    - *default_before_script
  script:
    - rm -rf robot/output
    - tox run-parallel -e "pytest,deps,robot"
  artifacts:
    expire_in: 1 week
    when: always
    paths:
      - robot/output

# 'deploy' stage
deploy:
  stage: deploy
  image: ${GITLAB_CI_IMAGE_DEBIAN}
  before_script:
    - *default_before_script
  script:
    - rm -rf dist
    - tox run -e "upload"
  only:
    refs:
      - tags
    variables:
      - $PYPI_USERNAME
      - $PYPI_PASSWORD

docker:
  stage: deploy
  variables:
    GNURADIO_IMAGE_TAG: 'satnogs'
    GITLAB_CI_DOCKER_IMAGE_VERSION: '${CI_COMMIT_REF_NAME}'
  rules:
    - if: $CI_COMMIT_TAG
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

docker_unstable:
  extends: docker
  variables:
    GNURADIO_IMAGE_TAG: 'satnogs-unstable'
    GITLAB_CI_DOCKER_IMAGE_VERSION: '${CI_COMMIT_REF_NAME}-unstable'
    GITLAB_CI_DOCKER_IMAGE_TAG_CACHE_NAME: 'cache-unstable'
    GITLAB_CI_DOCKER_IMAGE_TAG_LATEST_NAME: 'unstable'

pages:
  stage: deploy
  only:
    - tags

# 'sentry_release' stage
sentry_release:
  stage: sentry_release
  image: ${GITLAB_CI_IMAGE_SENTRY_CLI}
  script:
    - sentry-cli releases new --finalize -p ${CI_PROJECT_NAME} ${CI_PROJECT_NAME}@${CI_COMMIT_TAG}
    - sentry-cli releases set-commits --auto ${CI_PROJECT_NAME}@${CI_COMMIT_TAG}
  only:
    refs:
      - tags
    variables:
      - $SENTRY_AUTH_TOKEN
      - $SENTRY_ORG

# 'security' stage
dependency_scanning:
  stage: security
  needs: []
  variables:
    DS_EXCLUDED_ANALYZERS: 'gemnasium-maven'
sast:
  stage: security
  needs: []
secret_detection:
  stage: security
  needs: []
license_scanning:
  stage: security
  needs: []
container_scanning:
  stage: security
  needs:
    - job: docker
      artifacts: false
  variables:
    CI_APPLICATION_REPOSITORY: ${CI_REGISTRY_IMAGE}/${GITLAB_CI_DOCKER_IMAGE_NAME}
    CI_APPLICATION_TAG: ${CI_COMMIT_REF_NAME}
  rules:
    - if: $CI_REGISTRY_IMAGE && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
